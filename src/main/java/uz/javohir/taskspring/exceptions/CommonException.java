package uz.javohir.taskspring.exceptions;

import lombok.Data;

@Data
public class CommonException extends RuntimeException{
    Object data;
    String message;
    public CommonException(String message) {
        this.message = message;
    }
}
