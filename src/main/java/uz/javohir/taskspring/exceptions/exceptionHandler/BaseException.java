package uz.javohir.taskspring.exceptions.exceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import uz.javohir.taskspring.dto.responseItem.ResponseItem;
import uz.javohir.taskspring.exceptions.CommonException;
import uz.javohir.taskspring.security.jwt.JwtAuthException;

@ControllerAdvice
public class BaseException {
    private static final String STATUS_FAIL = "FAIL";
    private static final String STATUS_ERROR = "ERROR";

    @ExceptionHandler(CommonException.class)
    public ResponseEntity<ResponseItem> baseExceptionController(CommonException commonException){
        return getResponse(STATUS_FAIL, HttpStatus.CONFLICT, commonException.getMessage(), commonException.getData());
    }

    @ExceptionHandler(JwtAuthException.class)
    public ResponseEntity<ResponseItem> tokenExceptionController(JwtAuthException jwtAuthException){
        return getResponse(STATUS_FAIL, HttpStatus.INTERNAL_SERVER_ERROR, jwtAuthException.getMessage(), null);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ResponseItem> validationException(BadCredentialsException badCredentialsException){
        return getResponse(STATUS_FAIL, HttpStatus.FORBIDDEN, badCredentialsException.getMessage(), null);
    }

    protected ResponseEntity<ResponseItem> getResponse(String statusMessage, HttpStatus httpStatus,
                                                       String message, Object data){
        ResponseItem<Object> responseItem = new ResponseItem<>();
        responseItem.setData(data);
        responseItem.setMessage(message);
        responseItem.setStatus(statusMessage);
        responseItem.setHttpStatusCode(httpStatus.value());
        return ResponseEntity.status(HttpStatus.OK).body(responseItem);
    }

}
