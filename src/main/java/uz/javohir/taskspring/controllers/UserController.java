package uz.javohir.taskspring.controllers;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.javohir.taskspring.constants.MappingNames;
import uz.javohir.taskspring.controllers.base.BaseController;
import uz.javohir.taskspring.dto.UserDto;
import uz.javohir.taskspring.dto.responseItem.ResponseItem;
import uz.javohir.taskspring.services.UserService;

@RestController
@RequestMapping(value = MappingNames.Backend + MappingNames.User, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController extends BaseController {

    private final UserService userService;
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(
            value =  MappingNames.Registration,
            method = RequestMethod.POST
    )
    public ResponseEntity<ResponseItem<String>> registration(@RequestBody UserDto userDto){
        return success(userService.registration(userDto));
    }

    @RequestMapping(

            value = MappingNames.GetUsers,
            method = RequestMethod.GET
    )
    public ResponseEntity<ResponseItem<Object>> get(){
        return success(userService.getUsers());
    }

    @RequestMapping(
            value = MappingNames.Admin + MappingNames.DeleteUser,
            method = RequestMethod.DELETE
    )
    public ResponseEntity<ResponseItem<Object>> delete(@RequestParam Long id){
        return success(userService.deleteUser(id));
    }

    @RequestMapping(
            value = MappingNames.Admin + MappingNames.GetActiveUsers,
            method = RequestMethod.GET
    )
    public ResponseEntity<ResponseItem<Object>> getActiveUsers(){
        return success(userService.getActiveUsers());
    }

    @RequestMapping(
            value = MappingNames.Admin + MappingNames.GetInactiveUsers,
            method = RequestMethod.GET
    )
    public ResponseEntity<ResponseItem<Object>> getInActiveUsers(){
        return success(userService.getInActiveUsers());
    }

}
