package uz.javohir.taskspring.controllers.base;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import uz.javohir.taskspring.dto.responseItem.ResponseItem;

public abstract class BaseController {
    private <T> ResponseEntity<ResponseItem<T>> send(T data, String message, String status, HttpStatus httpStatusCode) {
        return new ResponseEntity<>(new ResponseItem<>(data, message, null, httpStatusCode.value()),
                getHeader(), httpStatusCode);
    }

    protected <T> ResponseEntity<ResponseItem<T>> success(T data) {
        return send(data, null, Status.SUCCESS.name(), HttpStatus.OK);
    }

    protected <T> ResponseEntity<ResponseItem<T>> fail(T data){
        return send(data, null, Status.FAIL.name(), HttpStatus.OK);
    }

    protected HttpHeaders getHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        return httpHeaders;
    }

    private enum Status {
        SUCCESS, FAIL, ERROR;
    }
}
