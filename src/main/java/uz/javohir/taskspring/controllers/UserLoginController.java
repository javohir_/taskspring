package uz.javohir.taskspring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uz.javohir.taskspring.bot.botApi.handler.UserProfileData;
import uz.javohir.taskspring.constants.MappingNames;
import uz.javohir.taskspring.controllers.base.BaseController;
import uz.javohir.taskspring.dto.UserAuthData;
import uz.javohir.taskspring.services.UserLoginService;

@RestController
@RequestMapping(value = MappingNames.Backend + MappingNames.Auth,
        produces = MediaType.APPLICATION_JSON_VALUE)

public class UserLoginController extends BaseController {

    @Autowired
    UserLoginService userLoginService;

    @RequestMapping(
            value = MappingNames.Login,
            method = RequestMethod.POST
    )
    public ResponseEntity login(@RequestBody UserProfileData userAuthDto) {
        return ResponseEntity.ok(userLoginService.login(userAuthDto));
    }
}
