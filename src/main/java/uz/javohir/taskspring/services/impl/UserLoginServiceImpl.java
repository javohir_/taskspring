package uz.javohir.taskspring.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import uz.javohir.taskspring.bot.botApi.handler.UserProfileData;
import uz.javohir.taskspring.constants.MsgCons;
import uz.javohir.taskspring.dto.UserAuthData;
import uz.javohir.taskspring.dto.responseItem.ResponseItem;
import uz.javohir.taskspring.dto.responseItem.UserAuthResponse;
import uz.javohir.taskspring.entities.User;
import uz.javohir.taskspring.repositories.UserRepo;
import uz.javohir.taskspring.security.jwt.JwtTokenProvider;
import uz.javohir.taskspring.services.UserLoginService;

@Service
public class UserLoginServiceImpl implements UserLoginService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepo userRepo;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Override
    public ResponseItem login(UserProfileData userAuthDto) {
        try {
            UserAuthResponse userAuthResp = new UserAuthResponse();
            ResponseItem responseItem = new ResponseItem();
            String username = userAuthDto.getUsername();
            User user = userRepo.findByUsername(username);
//            if (user == null) {
//                throw new CommonException(MsgCons.USER_NOT_FOUND);
//            }
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, userAuthDto.getPassword()));
            String token = jwtTokenProvider.createToken(username, user.getRoles());
            userAuthResp.setUsername(username);
            userAuthResp.setToken(token);

            responseItem.setData(token);
            responseItem.setMessage(null);
            responseItem.setStatus("OK");
            responseItem.setHttpStatusCode(200);
            return responseItem;
        } catch (AuthenticationException e) {
            throw new BadCredentialsException(MsgCons.INVALID_PASSWORD_OR_USERNAME);
        }
    }
}
