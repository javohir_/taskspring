package uz.javohir.taskspring.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import uz.javohir.taskspring.constants.MsgCons;
import uz.javohir.taskspring.dto.UserDto;
import uz.javohir.taskspring.entities.Role;
import uz.javohir.taskspring.entities.User;
import uz.javohir.taskspring.exceptions.CommonException;
import uz.javohir.taskspring.repositories.RoleRepo;
import uz.javohir.taskspring.repositories.UserRepo;
import uz.javohir.taskspring.services.UserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public String registration(UserDto userDto) {
        if (userDto == null) {
            throw new CommonException(MsgCons.INCORRECT_PARAM);
        }
        if (StringUtils.isEmpty(userDto.getUsername())) {
            throw new CommonException(MsgCons.INCORRECT_PARAM);
        } else if (userRepo.isUsernameExists(userDto.getUsername()) > 0) {
            throw new CommonException(MsgCons.USERNAME_EXISTS);
        }
        if (StringUtils.isEmpty(userDto.getPassword())) {
            throw new CommonException(MsgCons.PASSWORD_EMPTY);
        } else if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
            throw new CommonException(MsgCons.PASSWORD_NOT_EQUAL);
        }
        if (StringUtils.isEmpty(userDto.getPhone())) {
            throw new CommonException(MsgCons.INCORRECT_PARAM);
        } else if (userRepo.isPhoneExists(userDto.getPhone()) > 0) {
            throw new CommonException(MsgCons.PHONE_EXISTS);
        }

        Role roleUser = roleRepo.findByName("ROLE_USER");
        List<Role> roleList = new ArrayList<>();
        roleList.add(roleUser);

        final User user = new User();

        user.setUsername(userDto.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setConfirmPassword(bCryptPasswordEncoder.encode(userDto.getConfirmPassword()));
        user.setFullName(userDto.getFullName());
        user.setPhone(userDto.getPhone());
        user.setRoles(roleList);
        userRepo.save(user);

        return MsgCons.SUCCESS;
    }

    @Override
    public List<User> getUsers() {
        return userRepo.findAll();
    }

    @Override
    public String deleteUser(Long id) {
        if (id <= 0 && id == null) {
            throw new CommonException(MsgCons.INCORRECT_PARAM);
        }
        final User user = userRepo.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new CommonException(MsgCons.ALREADY_DELETED));
        user.setDeleted(true);
        userRepo.save(user);
        return MsgCons.SUCCESS;
    }

    @Override
    public User findByUsername(String username) {
        User user = userRepo.findByUsername(username);
        return user;
    }

    @Override
    public List<User> getActiveUsers() {
        List<User> users = userRepo.findUserByDeletedFalse();
        return users;
    }

    @Override
    public List<User> getInActiveUsers() {
        List<User> users = userRepo.findUserByDeletedTrue();
        return users;
    }
}
