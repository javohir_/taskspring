package uz.javohir.taskspring.services;

import uz.javohir.taskspring.dto.UserDto;
import uz.javohir.taskspring.entities.User;

import java.util.List;

public interface UserService {

    String registration(UserDto userDto);

    List<User> getUsers();

    String deleteUser(Long id);

    User findByUsername(String username);

    List<User> getActiveUsers();

    List<User> getInActiveUsers();

}
