package uz.javohir.taskspring.services;

import uz.javohir.taskspring.bot.botApi.handler.UserProfileData;
import uz.javohir.taskspring.dto.UserAuthData;
import uz.javohir.taskspring.dto.responseItem.ResponseItem;

public interface UserLoginService {
    ResponseItem login(UserProfileData userAuthDto);
}
