package uz.javohir.taskspring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.javohir.taskspring.constants.MsgCons;
import uz.javohir.taskspring.entities.User;
import uz.javohir.taskspring.exceptions.CommonException;
import uz.javohir.taskspring.security.jwt.JwtUser;
import uz.javohir.taskspring.security.jwt.JwtUserFactory;
import uz.javohir.taskspring.services.UserService;


@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username);

        if (user == null){
            throw new CommonException(MsgCons.USER_NOT_FOUND);
        }
        return JwtUserFactory.create(user);
    }
}
