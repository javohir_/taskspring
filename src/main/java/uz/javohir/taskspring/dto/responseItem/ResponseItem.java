package uz.javohir.taskspring.dto.responseItem;

import lombok.Data;

@Data
public class ResponseItem<T> {
    T data;
    private String message;
    private String status;
    private int httpStatusCode;

    public ResponseItem(T data, String message, String status, int httpStatusCode) {
        this.data = data;
        this.message = message;
        this.status = status;
        this.httpStatusCode = httpStatusCode;
    }

    public ResponseItem() {
    }
}
