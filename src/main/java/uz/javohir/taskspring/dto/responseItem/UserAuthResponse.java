package uz.javohir.taskspring.dto.responseItem;

import lombok.Data;

@Data
public class UserAuthResponse {
    private String username;
    private String token;
}
