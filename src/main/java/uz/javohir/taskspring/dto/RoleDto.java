package uz.javohir.taskspring.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RoleDto {

    @JsonProperty(value = "name")
    private String name;

    public RoleDto(String name) {
        this.name = name;
    }
}
