package uz.javohir.taskspring.dto;

import lombok.Data;

@Data
public class UserAuthData {

    private String username;
    private String password;

    public UserAuthData() {
    }
}
