package uz.javohir.taskspring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.javohir.taskspring.entities.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    List<User> findAll();

    @Query(
            "select count(username) from User " +
                    " where deleted = false " +
                    " and username = ?1 "
    )
    Long isUsernameExists(String username);

    @Query(
            "select count(phone) from User " +
                    " where deleted = false " +
                    " and phone = ?1 "
    )
    Long isPhoneExists(String phone);

    Optional<User> findByIdAndDeletedFalse(Long id);

    @Query(" select u from User u where u.username = ?1 ")
    User findByUsername(String username);

    List<User> findUserByDeletedFalse();

    List<User> findUserByDeletedTrue();

    Optional<User> findUserByUsername(String username);

}
