package uz.javohir.taskspring.entities;

import lombok.Data;
import uz.javohir.taskspring.constants.TableNames;
import uz.javohir.taskspring.entities.base.DataEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = TableNames.Users)
@Data
public class User extends DataEntity {

    private String username;
    private String password;
    private String confirmPassword;
    private String fullName;
    private String phone;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = TableNames.User_Roles,
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    public User(String username, String password, String confirmPassword, String fullName, String phone) {
        this.username = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.fullName = fullName;
        this.phone = phone;
    }

    public User() {
    }
}
