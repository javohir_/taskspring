package uz.javohir.taskspring.entities;

import lombok.Data;
import uz.javohir.taskspring.constants.TableNames;
import uz.javohir.taskspring.entities.base.DataEntity;

import javax.persistence.*;

@Entity
@Table(name = TableNames.Roles)
@Data
public class Role extends DataEntity {

    @Column(name = "role_name")
    private String name;

//    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
//    private List<User> users;
}
