package uz.javohir.taskspring.bot.botApi.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import uz.javohir.taskspring.bot.botApi.BotState;
import uz.javohir.taskspring.bot.botApi.InputMessageHandler;
import uz.javohir.taskspring.bot.cache.UserDataCache;
import uz.javohir.taskspring.dto.responseItem.ResponseItem;
import uz.javohir.taskspring.entities.User;
import uz.javohir.taskspring.repositories.UserRepo;
import uz.javohir.taskspring.services.UserLoginService;
import uz.javohir.taskspring.services.impl.UserLoginServiceImpl;

import java.util.Optional;

@Component
public class FillingAuthHandler implements InputMessageHandler {

    @Autowired
    private UserDataCache userDataCache;

    @Autowired
    private UserLoginServiceImpl userLoginService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public SendMessage handle(Message message) {
        if (userDataCache.getUsersCurrentBotState(message.getFrom().getId()).equals(BotState.FILLING_AUTH)) {
            userDataCache.setUsersCurrentBotState(message.getFrom().getId(), BotState.ASK_USERNAME);
        }
        return processUsersInput(message);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.FILLING_AUTH;
    }

    private SendMessage processUsersInput(Message inputMsg) {
        String usersAnswer = inputMsg.getText();
        int userId = inputMsg.getFrom().getId();
        long chatId = inputMsg.getChatId();
        String username = inputMsg.getFrom().getFirstName();

        UserProfileData profileData = userDataCache.getUserProfileData(userId);
        BotState botState = userDataCache.getUsersCurrentBotState(userId);

        SendMessage replyToUser = null;

        if (botState.equals(BotState.ASK_USERNAME)) {
            replyToUser = new SendMessage(String.valueOf(chatId), "Enter the username.");
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_PASSWORD);
        }

        if (botState.equals(BotState.ASK_PASSWORD)) {
            replyToUser = new SendMessage(String.valueOf(chatId), "Enter the password.");
            profileData.setUsername(usersAnswer);
            userDataCache.setUsersCurrentBotState(userId, BotState.AUTH_FILLED);
        }

        if (botState.equals(BotState.AUTH_FILLED)) {
            profileData.setPassword(usersAnswer);
            userDataCache.setUsersCurrentBotState(userId, BotState.ASK_AUTH);

            Optional<User> userOptional = userRepo.findUserByUsername(profileData.getUsername());

            if (userOptional.isPresent()){
                User user = userOptional.get();
                String uname = user.getUsername();
                if (profileData.getUsername().equals(uname) && passwordEncoder.matches(profileData.getPassword(), user.getPassword())){
                    replyToUser = new SendMessage(chatId, username + " " + "You signed in successfully!");

                }else {
                    replyToUser = new SendMessage(chatId, username + " " + " your username or password wrong )))))!");
                }
            }else {
                replyToUser = new SendMessage(chatId, "User not found");
            }
        }

        userDataCache.saveUserProfileData(userId, profileData);

        return replyToUser;
    }
}
