package uz.javohir.taskspring.bot.botApi;

public enum BotState {
    ASK_AUTH,
    ASK_USERNAME,
    ASK_PASSWORD,
    FILLING_AUTH,
    AUTH_FILLED,
    SHOW_MAIN_MENU,
    SHOW_HELP_MENU,
    SHOW_USER_DATA
}
