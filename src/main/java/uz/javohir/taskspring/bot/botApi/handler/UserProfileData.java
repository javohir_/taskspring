package uz.javohir.taskspring.bot.botApi.handler;

import lombok.Data;

@Data
public class UserProfileData {
    private String username;
    private String password;
}
