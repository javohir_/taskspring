package uz.javohir.taskspring.bot.botApi.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import uz.javohir.taskspring.bot.botApi.BotState;
import uz.javohir.taskspring.bot.botApi.InputMessageHandler;
import uz.javohir.taskspring.bot.cache.UserDataCache;

import java.util.ArrayList;
import java.util.List;

@Component
public class AskAuthHandler implements InputMessageHandler {
    @Autowired
    private UserDataCache userDataCache;

    @Override
    public SendMessage handle(Message message) {
        return processUsersInput(message);
    }

    @Override
    public BotState getHandlerName() {
        return BotState.ASK_AUTH;
    }

    private SendMessage processUsersInput(Message inputMsg) {
        int userId = inputMsg.getFrom().getId();
        String chatId = String.valueOf(inputMsg.getChatId());

        SendMessage replyToUser = new SendMessage(chatId, "Do you want to log in?");
        userDataCache.setUsersCurrentBotState(userId, BotState.FILLING_AUTH);

        replyToUser.setReplyMarkup(getInlineMessageButtons());
        return replyToUser;
    }

    private InlineKeyboardMarkup getInlineMessageButtons() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton buttonYes = new InlineKeyboardButton();
        buttonYes.setText("Yes");
        InlineKeyboardButton buttonNo = new InlineKeyboardButton();
        buttonNo.setText("No");

        //Every button must have callBackData, or else not work !
        buttonYes.setCallbackData("buttonYes");
        buttonNo.setCallbackData("buttonNo");

        List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
        keyboardButtonsRow1.add(buttonYes);
        keyboardButtonsRow1.add(buttonNo);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonsRow1);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;
    }

}
