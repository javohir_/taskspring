package uz.javohir.taskspring.bot.cache;

import uz.javohir.taskspring.bot.botApi.BotState;
import uz.javohir.taskspring.bot.botApi.handler.UserProfileData;

public interface DataCache {
    void setUsersCurrentBotState(int userId, BotState botState);

    BotState getUsersCurrentBotState(int userId);

    UserProfileData getUserProfileData(int userId);

    void saveUserProfileData(int userId, UserProfileData userProfileData);
}
