package uz.javohir.taskspring.bot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.javohir.taskspring.bot.botApi.TelegramFacade;

@Component
public class TelegramBot extends TelegramWebhookBot {
    @Autowired
    private TelegramFacade telegramFacade;

    private String webHookPath = "https://f8b83157fe26.ngrok.io";
    private String botUserName = "@javohir12345bot";
    private String botToken = "1756876870:AAFuwTUoqdATfEmPif0dtNR7PiOqa3VwaSc";

    @Override
    public String getBotUsername() {
        return botUserName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public String getBotPath() {
        return webHookPath;
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
        if (update.hasCallbackQuery()){
            System.out.println(update.getCallbackQuery().getFrom().getFirstName() + " : " + update.getCallbackQuery().getData());
        }else {

            System.out.println(update.getMessage().getFrom().getFirstName() + " : " + update.getMessage().getText());
        }
        BotApiMethod<?> replyMessageToUser = telegramFacade.handleUpdate(update);
        return replyMessageToUser;
    }
}
