package uz.javohir.taskspring.constants;

public interface MappingNames {
    String Backend = "/api";
    String User = "/user";
    String Registration = "/registration";
    String GetUsers = "/get";
    String DeleteUser = "/delete";
    String GetActiveUsers = "/activeUsers";
    String GetInactiveUsers = "/inactiveUsers";

    String Auth = "/auth";
    String Login = "/login";
    String Admin = "/admin";
 }
