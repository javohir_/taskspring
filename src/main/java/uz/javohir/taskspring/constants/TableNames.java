package uz.javohir.taskspring.constants;

public interface TableNames {
    String Users = "users";
    String Roles = "roles";
    String User_Roles = "user_roles";
}
