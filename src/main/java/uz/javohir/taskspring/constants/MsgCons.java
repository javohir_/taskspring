package uz.javohir.taskspring.constants;

public interface MsgCons {
    String INCORRECT_PARAM = "Incorrect param";
    String SUCCESS = "Success";
    String PHONE_EXISTS = "Phone already exists";
    String USERNAME_EXISTS = "Username already exists";
    String PASSWORD_EMPTY = "Password is empty";
    String PASSWORD_NOT_EQUAL = "Password not equal";
    String ALREADY_DELETED = "User already deleted";
    String USER_NOT_FOUND = "User not found";
    String INVALID_JWT_TOKEN = "JWT token is expired or invalid";
    String INVALID_PASSWORD_OR_USERNAME = "Invalid username or password";
 }
